# Coding Test Repos

* Welcome to your coding test repository. 
* This repo runs with `pnpm`,`turbo` & `react-script` 
* See below for instructions. 

## Install & Run 

* install:
```
pnpm install 
pnpm dev
```

* run the main script: 
```
npx ts-node packages/abortable-stream/src/runScript.ts
```


## Exercise objective and instructions 

* The objective is to create an "abortable stream generator function"
* This generator function is `abortableGenerator`, and need to : 
  - stream the files in `data/current/` folders one after the others
  - pipe this files stream to `stream-json` and yield one object at the time
* The `abortableGenerator` function can be aborted by a function, like the `defaultShouldAbortFn` example. 

### Requirement 1: implementation
* Please improve the `abortableGenerator` function to use streams and gracefully abord. Any other improvements of `shouldAbortFn` or a new, smarter `runScript` is welcome. 
* The following naive command will be used to evaluate you code : `npx ts-node packages/abortable-stream/src/runScript.ts`

### Requirement 2: Jest Test 

* Please add the Jest binary and a test file that verify a piece of your code. 


* Don't hesitate to reach me out for any question

