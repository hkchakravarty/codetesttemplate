
type DocumentStream = { id: string; title: string; };

export default function* abortableGenerator(shouldAbortFn: () => boolean): IterableIterator<DocumentStream> {

  // write here a generator function that : incrementally read the files in `data/current` as a stream piped to `stream-json`
  // This generator can be gracefully aborted at the fileStream level when shouldAbortFn return true

  const fakeDocument = { id: 'xxx', title: 'objects to retrive from data/current/chunks' }
  while (true) {
    if (shouldAbortFn()) throw new Error('This Error have to be replaced by an abort that gracefully stop the generator')
    yield fakeDocument
  }

}