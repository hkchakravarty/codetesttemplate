import React from "react"
import "./App.css"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="header">
          Docs
          <div className="Turborepo">React nest</div>
        </h1>
        <div>
          Write some React here if you want (not required)
          <span> | </span>
          A good README.md is required.
        </div>
      </header>
    </div>
  );
}

export default App;
